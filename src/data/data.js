import axios from 'axios'
var screen = 0;
var history = '';

function getscreen()
{
    return screen;
}

function setscreen(a){
    screen = a;
}

function gethistory(){
    return history;
}

async function setHistory(response){
    history = await JSON.parse(response);
    //console.log(history.history)
}

function updateHistory()
{
    const url = 'https://expressmyfxbook.herokuapp.com/gethistory';
    axios.get(url)
    .then(response => setHistory(response.request.response))
}

export {getscreen, setscreen, updateHistory, gethistory};