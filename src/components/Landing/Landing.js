import React from 'react'

export const LandingComponent = () => {
  const style = {
    page: {},
    layout: {},
    heading: {
      textAlign: 'center',
      fontSize: '48px',
      margin: '64px'
    }
  }

  return (
    <div style={style.page}>
      <div style={style.layout}>
        <img alt="Banner" width="100%" src="1-wtc-america-architecture-374710.jpg" />
      </div>
    </div>
  )
}
