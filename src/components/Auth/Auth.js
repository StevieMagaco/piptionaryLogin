import React from 'react'
import PropTypes from 'prop-types'

import { config } from 'aws-cognito-redux-saga'

class Auth extends React.Component {
  static propTypes = {
    getUser: PropTypes.func
  }

  componentWillMount() {
    config.config.set({
      region: 'us-east-1',
      IdentityPoolId: 'us-east-1:4e55ecc3-2f7b-4f41-8bb0-a3dcc5c788ab',
      UserPoolId: 'us-east-1_FmOyBBusF',
      ClientId: '3updru51gv82vksmdgbma3vru9'
    })

    this.props.getUser()
  }

  render() {
    return null
  }
}

export default Auth
