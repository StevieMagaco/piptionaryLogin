import React, { Component } from 'react';
import { setscreen, updateHistory } from '../../../../data/data';
//import MetisMenu from 'react-metismenu';
//import SideNav, { Toggle, Nav, NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
// Be sure to include styles at some point, probably during your bootstraping
//import '@trendmicro/react-sidenav/dist/react-sidenav.css';


class Sidebar extends Component {
    render() {
        return (
                <div className="sidebar">
                    <div className="sidebar-inner">
                        <a href="#" onClick={() =>  setscreen(0)}><i className="fa fa-fw fa-home"></i> Home</a>
                        <a href="#" onClick={() =>  setscreen(1)}><i className="far fa-list-alt"></i> Signal History</a>
                        <a href="#" onClick={() =>  setscreen(2)}><i className="fa fa-fw fa-wrench"></i> Subscription</a>
                        <a href="#"><i className="fa fa-fw fa-user"></i> Account</a>
                        <a href="#"><i className="fa fa-fw fa-envelope"></i> Contact</a>
                    </div>
                </div>
                )
            }            
}
export default Sidebar