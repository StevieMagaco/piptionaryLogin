import React, { Component } from 'react';
import { gethistory } from '../../../../data/data';
// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";

class Signal extends Component {
    render() {
        return (
                <div className="history-page">
                    <h1>History</h1>
                    <ReactTable
                      data={gethistory().history}
                      columns={[
                        {
                          Header: "Info",
                          columns: [
                            {
                              Header: "Open Time",
                              accessor: "openTime"
                            },
                            {
                              Header: "Close Time",
                              accessor: "closeTime"
                            },
                            {
                              Header: "Symbol",
                              accessor: "symbol"
                            },
                            {
                              Header: "Action",
                              accessor: "action"
                            },
                            {
                              Header: "Open Price",
                              accessor: "openPrice"
                            },
                            {
                              Header: "Close Price",
                              accessor: "closePrice"
                            },
                            {
                              Header: "Profit",
                              accessor: "profit"
                            },
                            {
                              Header: "Comment",
                              accessor: "comment"
                            }
                          ]
                        }
                      ]}
                      defaultPageSize={15}
                      className="-striped -highlight"
                    />
                </div>
                )
            }            
}
export default Signal