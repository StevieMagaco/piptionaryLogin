import React, { Component } from 'react';
import {PricingTable, PricingSlot, PricingDetail} from 'react-pricing-table';
import PayButton from '../../../StripePayment/stripeButton';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
const style = {
    heading: {
      textAlign: 'center',
      fontSize: '48px',
      margin: '64px'
    }
  }
class Subscription extends Component {
    render() {
        return (
                <div>
                    <div style={style.heading}></div>            
                    <h1 className="text-center">Subscription Plans</h1>
                    <PricingTable  highlightColor='#1976D2'>
                        <PricingSlot  onClick={this.submit} buttonText='SIGN UP' title='1 MONTH' priceText='$9.99'>
                            <PricingDetail> <b>15</b> projects</PricingDetail>
                            <PricingDetail> <b>5 GB</b> storage</PricingDetail>
                            <PricingDetail> <b>5</b> users</PricingDetail>
                            <PricingDetail strikethrough> <b>Time tracking</b></PricingDetail>
                            <div className="centerFlex buttonBox">
                            <PayButton email={cookies.get('cognitoeml')} amount={999}/>
                            </div>
                        </PricingSlot>
                        <PricingSlot highlighted onClick={this.submit} buttonText='SIGN UP' title='6 MONTH' priceText='$24.99'>
                            <PricingDetail> <b>35</b> projects</PricingDetail>
                            <PricingDetail> <b>15 GB</b> storage</PricingDetail>
                            <PricingDetail> <b>Unlimited</b> users</PricingDetail>
                            <PricingDetail> <b>Time tracking</b></PricingDetail>
                            <div className="centerFlex buttonBox">
                            <PayButton email={cookies.get('cognitoeml')} amount={2499}/>
                            </div>
                        </PricingSlot>
                        <PricingSlot  onClick={this.submit} buttonText='SIGN UP' title='1 YEAR' priceText='$99.99'>
                            <PricingDetail> <b>100</b> projects</PricingDetail>
                            <PricingDetail> <b>30 GB</b> storage</PricingDetail>
                            <PricingDetail> <b>Unlimited</b> users</PricingDetail>
                            <PricingDetail> <b>Time tracking</b></PricingDetail>
                            <div className="centerFlex buttonBox">
                            <PayButton email={cookies.get('cognitoeml')} amount={9999}/>
                            </div>
                        </PricingSlot>
                        <PricingSlot  onClick={this.submit}  title='2 YEAR' priceText='$200'>
                            <PricingDetail> <b>Unlimited</b> projects</PricingDetail>
                            <PricingDetail> <b>75 GB</b> storage</PricingDetail>
                            <PricingDetail> <b>Unlimited</b> users</PricingDetail>
                            <PricingDetail> <b>Time tracking</b></PricingDetail>
                            <div className="centerFlex buttonBox">
                            <PayButton email={cookies.get('cognitoeml')} amount={20000}/>
                            </div>
                        </PricingSlot>
                    </PricingTable>
                </div>
                )
            }            
}
export default Subscription