//import StripeCheckout from 'react-stripe-checkout';
import React from 'react'
import PropTypes from 'prop-types';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from '../StripePayment/CheckoutForm';
import { state } from 'aws-cognito-redux-saga';
import { getscreen, updateHistory } from '../../data/data';
/*Components*/
import Sidebar from './Components/Sidebar/Sidebar';
import Subscription from './Components/Subscriton/Subscription';
import Home from './Components/DashboardHome/Home';
import Signal from './Components/SignalHistory/Signal';


import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const style = {
  page: {},
  content: {
    display: 'flex',
    flexFlow: 'column',
    alignItems: 'center',
    width: '100%',
  },
  layout: {
    display: 'flex',
    flexFlow: 'row',
    //alignItems: 'center'
  },
  heading: {
    textAlign: 'center',
    fontSize: '48px',
    margin: '64px'
  },
  label: {
    fontSize: '24px',
    margin: '8px 0',
    color: 'rgb(0,64,128)'
  },
  token: {
    overflow: 'auto',
    overflowWrap: 'break-word',
    fontSize: '16px',
    width: '90vw'
  }
}

export class ProtectedComponent extends React.Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { page: 0 };
    this.changeComponent = this.changeComponent.bind(this);
  }
  
  static propTypes = {
    isSignedIn: PropTypes.string,
    auth: PropTypes.object
  }

  changeComponent(a){
    this.setState({page: a }, () => {console.log(this.state.page);})
  }
  

  componentDidMount(){
    console.log(this.props.user)
  }

  renderAuthInfo() {
    updateHistory();
    if(getscreen() == 0)
      return (  <Home/> )
    else if(getscreen() == 1)
      return (  <Signal/> )
    else if(getscreen() == 2)
      return (  <Subscription/> )
  }

  render() {
    const { auth } = this.props

    return (
      <div style={style.page}>
        <div style={style.layout}>
          <Sidebar changeFunc={this.changeComponent}/>
          <div className="Dashboard-content" style={style.content}>
            {auth.isSignedIn === state.AUTH_SUCCESS
              ? this.renderAuthInfo(auth)
              : null}
          </div>
        </div>
      </div>
    )
  }
}

      {/*<div style={style.token}>
        <div style={style.label}>Access Token</div>
        <div>{auth.info.accessToken.jwtToken}</div>
        <div style={style.label}>ID Token</div>
        <div>{auth.info.idToken.jwtToken}</div>
        <div style={style.label}>Refresh Token</div>
        <div>{auth.info.refreshToken.token}</div>
        </div>*/}
        
      {/*<Router>
         <div>
         <Route exact path="/" component={Home} />
         <Route path="/subscription" component={Subscription} />
         <Route path="/signalhistory" component={Signal} />
         </div>
         </Router>*/}