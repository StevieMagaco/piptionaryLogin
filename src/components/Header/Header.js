import React from 'react'
import PropTypes from 'prop-types'

import { Toolbar, ToolbarGroup } from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import FlatButton from 'material-ui/FlatButton'

import Cookies from 'universal-cookie';

import { Link } from 'react-router-dom';

import { state } from 'aws-cognito-redux-saga';

const cookies = new Cookies();

export default class HeaderComponent extends React.Component {
  static propTypes = {
    isSignedIn: PropTypes.string,
    signUpError: PropTypes.bool,
    signOut: PropTypes.func,
    signIn: PropTypes.func,
    signUp: PropTypes.func,
    auth: PropTypes.object
  }

  signOut = () => {
    this.props.signOut()
  }

  render() {
    const { auth } = this.props

    return (
      <div className="header-container">
        <Toolbar>
          <ToolbarGroup>
            <a href="/"><img className="homelogo" alt="github" src="piptionary_logo-nobg.png" /></a>
            <FlatButton
              label="Dashboard"
              containerElement={<Link to="/" />}
            />
          </ToolbarGroup>

          <ToolbarGroup>
            {auth.isSignedIn !== state.AUTH_SUCCESS ? (
              <FlatButton
                className="dashboardButton"
                containerElement={<Link to="/signin" />}
                label="Sign Up / Sign In"
                onClick={this.signIn}
              />
            ) : (
              <div>
                <span className="hearedEmail" >{cookies.get('cognitoeml')/*auth.info.username*/}</span>
                <img className="signedInIcon" alt="Logged In" width="28" src="baseline_check_circle_black_36dp.png" />
                <FlatButton label="Sign Out" onClick={this.signOut} />
              </div>
            )}
          </ToolbarGroup>
        </Toolbar>
      </div>
    )
  }
}
