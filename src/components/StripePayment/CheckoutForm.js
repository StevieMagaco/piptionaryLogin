import React, {Component} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.state = {complete: false};
    this.submit = this.submit.bind(this);
  }

  async submit(ev) {
    // User clicked submit
    let {token} = await this.props.stripe.createToken({name: cookies.get('cognitoeml')});
    console.log(token.id, cookies.get('cognitoeml'));
  }

  render() {

    if (this.state.complete) return <h1>Purchase Complete</h1>;

    return (
      <div className="checkout">
        <p>Would you like to complete the purchase?</p>
        <div className="stripePaymentBlock">
          <CardElement />
          <div className="centerButtonContainer">
            <button onClick={this.submit} className="btn waves-effect waves-light" type="submit" name="action">Submit
              <i className="material-icons right">send</i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default injectStripe(CheckoutForm);